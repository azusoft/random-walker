#Range

class Range
  def bounding(val)
    [[val, first].max, last].min
  end
end