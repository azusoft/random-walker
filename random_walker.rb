#Basic file

require 'gosu'
load 'range.rb'

WINDOW_HEIGHT = 320
WINDOW_WIDTH = 568

class Window < Gosu::Window
  def initialize
    super WINDOW_WIDTH, WINDOW_HEIGHT, false, 1
    self.caption = "Random walker"
    @walker = Walker.new
  end

  def update
    @walker.walk
  end

  def draw
    @walker.draw
  end

end

class Walker
  attr_accessor :x, :y
  def initialize(x = 50, y = 50)
    @x = x
    @y = y
  end

  def walk
    @x = (0..WINDOW_WIDTH).bounding(@x + [-1, 0, 1].sample)
    @y = (0..WINDOW_HEIGHT).bounding(@y + [-1, 0, 1].sample)
  end

  def draw
    Gosu.draw_rect(@x, @y, 2, 2, Gosu::Color.new(0xffffffff))
  end
end

window = Window.new
window.show